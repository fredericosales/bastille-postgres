# Bastille Postgresql13

---

#### Bastille file vars

	VERSION		default value 13
	USER	    default value master
	DB          default value blaster
	PASSWORD    default value DDFFAA00#

#### How to use

```bash
[root@manunggul ~/] #: bastille template [jail] https://github.com/infracead/postgresql
```

---

#### Changing version


```bash
root@manunggul [ /usr/local/etc ] #: bastille template [jail] https://gitlab.com/fredericosales/bastile-postgresql --arg VERSION=12
```


#### Changins user and password

```bash
root@manunggul [ /usr/local/etc ] #: bastille template [jail] https://gitlab.com/fredericosales/bastile-postgresql --arg USER=asa --arg PASSWORD=akira
```

---
